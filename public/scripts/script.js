$(document).ready( () => {

})
function activeThis(el) {
    $(".navbar-item").attr("class", 'navbar-item');
    $(el).attr('class', 'navbar-item active');
}

function toggleContent(id, position="left") {
    if($(`#${id}`).attr("class") == "toggle-content-active"){
        $(`[id^=content-${position}]`).attr("class", "toggle-content")
        $(`[id^=button-content-${position}]`).attr("class", "fa fa-caret-down")
    }
    else{
        $(`[id^=content-${position}]`).attr("class", "toggle-content")
        $(`[id^=button-content-${position}]`).attr("class", "fa fa-caret-down")
        $(`#${id}`).attr("class", "toggle-content-active");
        $(`#button-${id}`).attr("class", "fa fa-caret-up");
    }
}